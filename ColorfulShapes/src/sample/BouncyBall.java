package sample;

/**
 Not ready: But an fun little project to make a bouncy ball fly around a screen. Little Buggy Atm, but kinda
 a entertaining program to run. There is a bouncing red ball on the screen, and to close the program, one needs
 to click it in time, otherwise, it might fly away and get lost!

 **/
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class BouncyBall extends Application {

    @Override
    public void start(Stage stage) {

        Pane canvas = new Pane();
        Scene scene = new Scene(canvas, 800, 550, Color.BLACK);
        Circle ball = new Circle(24, Color.RED);    // This is our little red ball, size and color configurable
        ball.relocate(30, 30);

        canvas.getChildren().add(ball);

        stage.initStyle(StageStyle.DECORATED);
        scene.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Platform.exit();
                System.exit(0);
            }
        });

        stage.setTitle("Grab the Bouncy Ball Before it gets away!! [Just a for fun project] ");
        stage.setScene(scene);
        stage.show();

        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(20), new EventHandler<ActionEvent>() {

            // Factors to control Speed
            double speedX = 12; // Horizontal Speed for the Ball
            double speedY = 10; // Vertical Starting speed for the ball

            @Override
            public void handle(ActionEvent t) {

                ball.setLayoutX(ball.getLayoutX() + speedX);
                ball.setLayoutY(ball.getLayoutY() + speedY);

                Bounds bounds = canvas.getBoundsInLocal();

                // Bouncing: When the ball hits the Borders of the screen, it needs to bounce
                // This program does that by reversing the direction of the balls, according to velocity

                if (ball.getLayoutX() <= (bounds.getMinX() + ball.getRadius()) ||
                        ball.getLayoutX() >= (bounds.getMaxX() - ball.getRadius())) {

                    // Safe but boring constant velocity Bounces (Less buggy, but less entertaining)
                    //speedX = -speedX;

                    // Experimental RNG Bouncing
                    double random = (Math.random() * 100 + 1);
                    double bounce = random % 0.50; // Randomly speeds up ball
                    speedX = -((bounce + 0.8) * speedX);


                }

                // Bounces for the Top and Bottom Panes
                if ((ball.getLayoutY() >= (bounds.getMaxY() - ball.getRadius())) ||
                        (ball.getLayoutY() <= (bounds.getMinY() + ball.getRadius()))) {


                    // Safe but boring constant velocity Bounces (Less buggy, but less entertaining)
//                    dy = -dy;

                    // Experimental RNG Bouncing
                    double random = (Math.random() * 50 + 1);
                    double bounce = random % 0.50; // Randomly speeds up ball
                    speedY = -((bounce + 0.8) * speedY);


                }

            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    public static void main(String[] args) {
        launch();
    }
}