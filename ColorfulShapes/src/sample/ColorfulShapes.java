package sample;


// Imports

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import static java.lang.Math.random;


public class ColorfulShapes extends Application {

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root, 800, 600, Color.BLACK);
        primaryStage.setScene(scene);

        primaryStage.setTitle("Colorful Shapes JavaFX");


        Group shapes = new Group();

        // Adds our Circles to the stage
        for (int i = 0; i < 30; i++) {  // (Controls how many, this adds 30 circles i think)
            Circle circle = new Circle(150, Color.web("white", 0.05));  // (Color and opacity of Fill
            circle.setStrokeType(StrokeType.OUTSIDE);
            circle.setStroke(Color.web("white", 0.26)); // (Color of the outer outline)
            circle.setStrokeWidth(4);
            shapes.getChildren().add(circle);
        }

        // Here, this code adds in 30 rectangles as well. (can be uncommented to have solely Circles added, or modified for others)
        // The same principle can be applied to other shapes as well! You can add in Circles and rectangles and whatever shapes you want!
        for (int i = 0; i < 30; i++) {
            Rectangle shape = new Rectangle(50, 50, Color.web("white", 0.20));
            shape.setStrokeType(StrokeType.OUTSIDE);
            shape.setStroke(Color.web("white", 0.80));
            shape.setStrokeWidth(4);
            shapes.getChildren().add(shape);
        }

        // Setting up the colors for our Gradient
        Rectangle colors = new Rectangle(scene.getWidth(), scene.getHeight(),
                new LinearGradient(0f, 1f, 1f, 0f, true, CycleMethod.NO_CYCLE, new
                        Stop[]{
                        new Stop(0, Color.web("#f8bd55")),
                        new Stop(0.14, Color.web("#c0fe56")),
                        new Stop(0.28, Color.web("#5dfbc1")),
                        new Stop(0.43, Color.web("#64c2f8")),
                        new Stop(0.57, Color.web("#be4af7")),
                        new Stop(0.71, Color.web("#ed5fc2")),
                        new Stop(0.85, Color.web("#ef504c")),
                        new Stop(1, Color.web("#f2660f")),}));
        colors.widthProperty().bind(scene.widthProperty());
        colors.heightProperty().bind(scene.heightProperty());

        Group blendModeGroup =
                new Group(new Group(new Rectangle(scene.getWidth(), scene.getHeight(),
                        Color.BLACK), shapes), colors);
        colors.setBlendMode(BlendMode.OVERLAY);
        root.getChildren().add(blendModeGroup);


        // Sets a blur effect over our shapes. Can be disabled for crisp images or cool effects
        //shapes.setEffect(new BoxBlur(10, 10, 3));     // 3 layers of Blur effects, can be swapped
        shapes.setEffect(new BoxBlur(5, 10, 1));   // 1 layer of Blur effects, my favorite

        Timeline timeline = new Timeline();

        for (Node circle : shapes.getChildren()) {
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO, // set start position at 0
                            new KeyValue(circle.translateXProperty(), random() * 800),
                            new KeyValue(circle.translateYProperty(), random() * 600)
                    ),
                    new KeyFrame(new Duration(60000), // Provides First 60 S of animation
                            new KeyValue(circle.translateXProperty(), random() * 1500),
                            new KeyValue(circle.translateYProperty(), random() * 1500)
                    ),
                    new KeyFrame(new Duration(120000), // Provides 60-120 S of animation
                            new KeyValue(circle.translateXProperty(), random() * 800),
                            new KeyValue(circle.translateYProperty(), random() * 600)
                    ),
                    new KeyFrame(new Duration(180000), // Provides next 120-180 S of animation
                            new KeyValue(circle.translateXProperty(), random() * 800),
                            new KeyValue(circle.translateYProperty(), random() * 600)
                    ),
            new KeyFrame(new Duration(18000000), 
                    new KeyValue(circle.translateXProperty(), random() * 8000),
                    new KeyValue(circle.translateYProperty(), random() * 6000)
            )

            );
        }

// Plays the animation
        timeline.play();

        primaryStage.show();
    }

    public static void main(String[] arguments) {
        Application.launch(arguments);
    }
}
